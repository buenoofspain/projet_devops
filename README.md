# __Projet DevOps__

![](pictures/schema_général.png)

## __Required__

- Vagrant
- Virtualbox
- Java
- Maven
- Jenkins
- Tomcat
- Docker
- Docker-compose
- SonarQube
- SonarScanner
- Artifactory

See installation details in [Required file](https://gitlab.com/buenoofspain/projet_devops/-/blob/main/required.md)


## __Infrastructure__

### __Liste des machines__

|Machines        |dédiée : `jenkins`         |déploiement : `app`               |DB & monitoring : `supervision`|K8S : `masternode`         |K8S :  `workernode1`       |K8S :  `workernode2`       |
| :------------: | :-----------------------: | :------------------------------: | :---------------------------: | :------------------------:| :-----------------------: | :-----------------------: |
|Ressources RAM  |6 Go                       |4 Go                              |2 Go                           |4 Go                       |2 Go                       | 2 Go                      |
|Ressources CPU  |2                          |1                                 |1                              |2                          |1                          | 1                         |
|Forwarded port 1|8080 -> 9000 (Jenkins)     |8080 -> 9001 (Petclinic in Tomcat)|3306 -> 9003 (MySQL)           |9100 -> 9017 (NodeExporter)|9100 -> 9018 (NodeExporter)|9100 -> 9019 (NodeExporter)|
|Forwarded port 2|9000 -> 9005 (SonarQube)   |8081 -> 9004 (Petclinic in Docker)|9090 -> 9011 (Prometheus)      |                           |30080 -> 9008 (petclinic service)    |30080 -> 9009 (petclinic service)                           |
|Forwarded port 3|9100 -> 9015 (NodeExporter)|9100 -> 9014 (NodeExporter)       |3000 -> 9012 (Grafana)         |                           |                           |                           |
|Forwarded port 4|                           |                                  |9100 -> 9016 (NodeExporter)    |                           |                           |                           |
|Forwarded port 5|                           |                                  |8086 -> 9020 (InfluxDB)        |                           |                           |                           |



Pour le détail de l'infrastructure, voir le [Vagrantfile](/ressources/vagrantfiles).
Pour le détail du projet et notamment les fichiers [pom.xml](https://github.com/FrancoisSevestre/spring-framework-petclinic/blob/master/pom.xml) et [Dockerfile](https://github.com/FrancoisSevestre/spring-framework-petclinic/blob/master/Dockerfile) voir le [GitHub](https://github.com/FrancoisSevestre/spring-framework-petclinic/).

### Déploiement de l'infrastructure avec les rôles Ansible

- Installer Ansible sur la machine de base
- Crér des roles Ansible 
  ```
  mkdir roles && cd roles && ansible-galaxy init <NomduRole>
  ```
  - Rôles créés pour le projet :
    - javaInstall : Installation de openjdk-11
    - mavenInstall : Installation de Maven
    - tomcatInstall : Installation et lancement de Tomcat
    - dockerInstall : Installation et configuration de Docker
    - jenkinsInstall : Installation de jenkins
    - sonarqubeInstall : Installation de SonarQube et SonarScanner
    - kubernetesInstall : Installation de Kubernetes et déploiement
    - masternodeSetup : Configuration de masternode
    - workernodeSetup : Configuration des workernodes
    - masternodeControl : Vérification de la jointure des workernodes sur le masternode
    - deployDockerImage : Launch docker images (MySQL, Grafana, Prometheus)



- Détail du déploiement du Playbook Ansible [start.yml](https://gitlab.com/buenoofspain/projet_devops/-/blob/main/ressources/ansible/start.yml) pour les machines traitées par groupes (voir [hosts](https://gitlab.com/buenoofspain/projet_devops/-/blob/main/ressources/ansible/hosts)) :

| Machines                                       | `jenkins` | `app` | `supervision` | `masternode` | `workernode1` | `workernode2` |
| :--------------------------------------------: | :-------: | :---: | :-----------: | :-----------:| :-----------: | :-----------: |
| Install Java (et Maven)                        | X         | X     |               |              |               |               |
| Install Tomcat                                 |           | X     |               |              |               |               |
| Install Docker                                 | X         | X     | X             | X            | X             | X             |
| Install Jenkins (et SonarQube et SonarScanner) | X         |       |               |              |               |               |
| Install Kubernetes                             |           |       |               | X            | X             | X             |
| Setup masternode server                        |           |       |               | X            |               |               |
| Setup workernode servers                       |           |       |               |              | X             | X             |
| Control join workernodes to masternode         |           |       |               | X            |               |               |
| Deploy MySQL DB and Supervised servers         |           |       | X             |              |               |               |
| Deploy NodeExporter                            | X         | X     | X             | X            | X             | X             |


## __Configuration et déploiement de l'infrastructure détaillées__

### __Lancement des machines virtuelles__

Installer Vagrant et lancer Vagrant via le Vagrantfile :
`cd ressources/vagrantfiles && vagrant up`

Vérification de l'état des machines : 
```
formation@151-106-9-1:~/Documents/projet/projet_devops/ressources/vagrantfiles$ vagrant global-status
id       name      provider   state   directory                                                              
-------------------------------------------------------------------------------------------------------------
d597a15  jenkins   virtualbox running /home/formation/Documents/projet/projet_devops/ressources/vagrantfiles 
4747cc0  petclinic virtualbox running /home/formation/Documents/projet/projet_devops/ressources/vagrantfiles 
2c8e7c9  archivage virtualbox running /home/formation/Documents/projet/projet_devops/ressources/vagrantfiles 
```

### __Serveur jenkins__

Installer :
- Java-JDK 11
- Maven
- Jenkins
- Docker
- SonarQube
- SonarScanner

#### _Paramétrer Jenkins_

- accéder à Jenkins : `http://192.168.60.10:8080/`
- récupérer le mot de passe temporaire dans `/var/lib/jenkins/secrets/initialAdminPassword`
- changer le mot de passe
- ajout de plugins: `Git plugin`, `GitHub Integration`, `Maven Invoker`, `Maven Integration`, `Pipeline`, `Pipeline: Basic Steps`, `SSH Agent Plugin `, `Publish Over SSH`, `Deploy to container`, `Docker Compose Build Step`, `SonarQube Scanner for Jenkins`, `Artifactory`
- ajouter le JDK dans la configuration globale : `JAVA-11`
  - décocher l'installation automatique
  - ajouter le path : `/usr/lib/jvm/java-11-openjdk-amd64`
- ajouter Maven dans la configuration globale: `Maven3`
  - décocher l'installation automatique
  - ajouter le path `/opt/maven`


#### _Paramétrer SonarQube_
  - Dans SonarQube (port 9005) : 
    - Créer un nouveau projet : <Manually>
    - Name: sonarQ
    - Générer un token 
    - Choisir Maven 
    - Copier les commandes pour les ajouter au pipeline jenkins : 
      ```
      stage('Build') {
            steps {
                withSonarQubeEnv('Sonar') {
                    sh 'mvn package sonar:sonar \
                        -Dsonar.projectKey=sonarQ \
                        -Dsonar.host.url=http://151.106.xxx.xxx:9005 \
                        -Dsonar.login=xxx'
                }
            }
        }
      ```
  - Dans Jenkins > Configuration > SonarQube servers :
    - Name: Sonar
    - Server URL:  http://192.168.60.10:9005/
    - Sevrer authentification token: 
      - Add un "Secret text" et ajouter le token de SonarQube généré auparavant
      - Sélectionner le token et "Save"
  - Dans Jenkins > Global Tool Configuration > SonarQube Scanner:
    - "Add SonarQube Scanner"
      - Name: SONAR_4.5
      - SONAR_RUNNER_HOME: /opt/sonar-scanner-4.5
      - "Save"


#### _Credential configuration_

Créer les credentials pour "deployer" : 
  - Scope: par défaut
  - Username: deployer
  - Password: deployer
  - ID: deployer
  - Description: Deployer to Tomcat

Créer les credentials pour l'accès à DockerHub : 
  - Scope: par défaut
  - Username: projetdevopsfn
  - Password: xxx
  - ID: projetdevopsfn
  - Description: Deployer to Tomcat


### __Serveur app1__

Installer Java-JDK 11, Tomcat et Docker

#### _Test de déploiement de l'image Docker_

Créer et démarrer le conteneur tomcat manuellement via le server Jenkins : 
- `docker -H tcp://192.168.60.20:2375 run -p 8081:8080 -d --name dockerized-app projetdevopsfn/petclinic`
Puis supprimer le conteneur : 
- `sudo docker -H tcp://192.168.60.20:2375 rm -f $(docker -H tcp://192.168.60.20:2375 ps -a -q)`

Ou en local :
- `docker run -p 8081:8080 -d --name dockerized-app projetdevopsfn/petclinic`
- `sudo docker rm -f $(docker ps -a -q)`


![](pictures/application.png)


#### _Déployer l'image docker pré-requis au pipeline Jenkins_

Avec le pipeline Jenkins:
- Démarrer Tomcat : `sudo /usr/local/bin/tomcatup`
- Arrêter docker : `sudo systemctl stop docker.service`
- Exposer la socket de docker: `sudo dockerd -H unix:///var/run/docker.sock -H tcp://0.0.0.0`


### __Serveur archivage/supervision__

- Exécuter l'image Docker : `docker run -e MYSQL_USER=petclinic -e MYSQL_PASSWORD=petclinic -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=petclinic -p 3306:3306 mysql:5.7.8`
- Modifier le fichier pom.xml (voir commit)
- Modifier le pipeline pour considérer la base de données MySQL : `-P MySQL`


### _Cluster Kubernetes__

Nous avons créer deux autres serveurs : un master et un worker
Pour l'installation voir [Required file](https://gitlab.com/buenoofspain/projet_devops/-/blob/main/required.md).

Créer une clé ssh dans le serveur jenkins
`ssh-keygen`
Copier la clé dans le master :
`ssh-copy-id vagrant@192.168.60.40`
Vérifier avec :
`ssh vagrant@192.168.60.40`

Dans Jenkins, installer le module `publish over ssh` : 
- Configure du système :
  - Publish over SSH : 
    - Ajouter la clé privé (voir `cat ~/.ssh/id_rsa`)
    - Name : masternode
    - Hostname : 192.168.60.40
    - Username : vagrant
    - Remote Directory : /home/vagrant

Modifier le pipeline


### __Supervisions__

Dans le serveur archivage/supervision : on lance 2 conteneur (un pour prometheus et un pour grafana).
Il mappe les ports pour prometheus (9090 -> 9011) et grafana (3000 -> 9012).

Lancer le conteneur grafana :
```
docker run -d --name=grafana -p 3000:3000 grafana/grafana
```
Configuration de prometheus :
```
mkdir -p prometheus/etc
mkdir -p prometheus/data
chmod 777 data/
cd prometheus/
vim etc/prometheus.yml
```
Editer le fichier de configuration :
```
global:
  scrape_interval: 15s
scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']
```
Lancer le conteneur prometheus :
```
docker run --name prometheus -v $PWD/etc/:/etc/prometheus/ -v $PWD/data/:/prometheus/ -d -p 9090:9090 prom/prometheus
```


Se connecter à Grafana (port 9012) et paramétrer les outils (Settings > Data Sources) :

- Prometheus :
 - Name: Prometheus
 - URL: http://151.106.9.1:9011
 - Access: Server (default)
 - Save & Test

- MySQL : 
 - Name: MySQL
 - Host: 151.106.9.1:9003
 - Database: petclinic
 - User: petclinic; Password: petclinic
 - Skip TLS Verify: cocher
 - Save & Test

- InfluxDB: 
 - Name: InfluxDB
 - Query Language: Flux
 - URL: http://151.106.9.1:9020/
 - Access: Server (default)
 - Basic auth: cocher
 - User: jenkins
 - Passwork: jenkins123
 - Organization: 
 - Token: 
 - Save & Test



Editer le fichier de configuration de prometheus suite au déploiement de Node Exporter :
```
global:
  scrape_interval: 15s
scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9090']
  - job_name: node_exporter
    static_configs:
      - targets:
         - '151.106.16.69:9014'
         - '151.106.16.69:9015'
         - '151.106.16.69:9016'
         - '151.106.16.69:9017'
         - '151.106.16.69:9018'
         - '151.106.16.69:9019'
```

Utiliser InfluxDB pour récupérer des données sur jenkins :
- Exécuter l'image Docker : `docker run -d -p 8086:8086 -e DOCKER_INFLUXDB_INIT_USERNAME=jenkins       -e DOCKER_INFLUXDB_INIT_PASSWORD=jenkins123       -e DOCKER_INFLUXDB_INIT_ORG=jenkins       -e DOCKER_INFLUXDB_INIT_BUCKET=jenkins       influxdb:2.0`
- Installer le plugin Jenkins `InfluxDB Plugin`
- Se connecter sur le port et configurer
  - Data > Load Data > Generate Token > All access token
    -> Grafana
    -> jenkins
    -> jenkins123
    -> Copier le token de l'organization
- Créer des Credentials:
  -> jenkins
  -> jenkins123 token
  -> jenkins
- Paramétrer le plugin InfluxDB sur Jenkins
  -> Description: jenkins
  -> URL: http://151.106.9.1:9020
  -> Credentials: jenkins/******** (influxdb)
  -> Organization : jenkins
  -> Database/Bucket: jenkins
  -> Cocher "Expose Exceptions" ---> Non, ne rien cocher
- Compléter le pipeline :
  ```
        stage('to influxdb') {
            steps {
        influxDbPublisher customPrefix: '', customProjectName: '', jenkinsEnvParameterField: '', jenkinsEnvParameterTag: '', selectedTarget: 'jenkins'
            }
        }
  ```
- Configurer InfluxDB dans Grafana (voir plus haut)
- Créer les pannels nécessaires

### Machine de base

Installer NodeExporter




## __Chaîne CI/CD__

### __Pipeline manuel dans Jenkins__

![](pictures/jenkins_deploy.png)

```
pipeline {
    agent any

    tools {
        // Install the Maven version configured as "M3" and add it to the path.
        maven "Maven3"
    }

    stages {
        stage('SCM') {
            steps {
                // Get some code from a GitHub repository
                //git url: 'https://github.com/spring-petclinic/spring-framework-petclinic.git'
                git url: 'https://github.com/FrancoisSevestre/spring-framework-petclinic'
            }
        }
        
        stage('Build and MySQL database') {
            steps {
                // Run Maven on a Unix agent.
                sh "mvn package -P MySQL"
            }
        }
        
        stage('Code check') {
            steps {
                // Run Maven on a Unix agent.
                withSonarQubeEnv('Sonar') {
                // some block
                    sh "mvn sonar:sonar \
                    -Dsonar.projectKey=sonarQ \
                    -Dsonar.host.url=http://151.106.9.1:9005 \
                    -Dsonar.login=a5274bcb538f3e433b336a65cf0fb9c4802978c2"
                } 
            }
        }
        
        stage('Local Deploy'){
            steps{
                deploy adapters: [tomcat8(credentialsId: 'deployer', 
                                  path: '', 
                                  url: 'http://192.168.60.20:8080/')], 
                        contextPath: null, 
                        war: '**/*.war'
            }
        }
        
        stage('Image building') {
            steps {
                sh 'docker image build -t projetdevopsfn/petclinic .'
            }
        }
        
        stage('Push to Docker Hub') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'projetdevopsfn', passwordVariable: 'dockerhub', usernameVariable: 'idDockerHub')]) {
                // some block
                    sh 'docker login -u $idDockerHub -p $dockerhub'
                    sh 'docker tag projetdevopsfn/petclinic projetdevopsfn/petclinic:v$BUILD_NUMBER'
                    sh 'docker push projetdevopsfn/petclinic:v$BUILD_NUMBER'
                    sh 'docker tag projetdevopsfn/petclinic projetdevopsfn/petclinic:latest'
                    sh 'docker push projetdevopsfn/petclinic:latest'
                }
            }
        }
        
        stage('Image Deploy') {
            steps{
                sh 'docker -H tcp://192.168.60.20:2375 rm -f $(docker -H tcp://192.168.60.20:2375 ps -a -q)'
                sh 'docker -H tcp://192.168.60.20:2375 system prune -f'
                sh 'docker -H tcp://192.168.60.20:2375 run -p 8081:8080 -d --name dockerized-app projetdevopsfn/petclinic'
                sh 'docker -H tcp://192.168.60.20:2375 pull projetdevopsfn/petclinic'
            }
        }
        

        stage('Deploy to kubernetes') {
            steps {
                sshPublisher(publishers: [sshPublisherDesc(configName: 'masternode', transfers: [sshTransfer(cleanRemote: false, excludes: '', execCommand: '', execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: '', remoteDirectorySDF: false, removePrefix: '', sourceFiles: '**/deploy.yml')], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false)])
                sshPublisher(publishers: [sshPublisherDesc(configName: 'masternode', transfers: [sshTransfer(cleanRemote: false, excludes: '', execCommand: 'sudo kubectl apply -f deploy.yml', execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: '', remoteDirectorySDF: false, removePrefix: '', sourceFiles: '')], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: false)])
            }
        }

        stage('to influxdb') {
            steps {
        influxDbPublisher customPrefix: '', customProjectName: '', jenkinsEnvParameterField: '', jenkinsEnvParameterTag: '', selectedTarget: 'jenkins'
            }
        }
    }
}


```

### __Test de fonctionnement__
Déploiement dans Tomcat :
http://151.106.9.1:9001/petclinic/
http://151.106.16.69:9001/petclinic/

Déploiement de l'image Docker (via DokerHub) :
http://151.106.9.1:9004/
http://151.106.16.69:9004/
